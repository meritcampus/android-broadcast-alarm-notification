package com.example.broadcastalarmnotification;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button btn=(Button) findViewById(R.id.ok);
		btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ok:
			EditText text = (EditText) findViewById(R.id.time);
		    int time = Integer.parseInt(text.getText().toString());
		    
		   
		    Intent notificationIntent = new Intent(this, BroadCastAlarmNotification.class);
	       
	        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent,0);

	        Calendar c = Calendar.getInstance();
	        c.add(Calendar.SECOND, time);
	        long firstTime = c.getTimeInMillis();
	        // Schedule the alarm!
	        AlarmManager am = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
	        am.set(AlarmManager.RTC_WAKEUP, firstTime, pendingIntent);
		    Toast.makeText(this, "Alarm set in " +time+ " seconds",
		        Toast.LENGTH_LONG).show();
			break;

		default:
			break;
		}
		
	}
	
	
}